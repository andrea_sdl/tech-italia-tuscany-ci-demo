var assert = require('assert');
var request = require('supertest');
var express = require('express');
var path = require('path');
var index = require('../routes/index');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app;

beforeEach(function () {
    app = express();

    // view engine setup
    app.set('views', path.join(__dirname, '../views'));
    app.set('view engine', 'jade');
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));
    app.use('/', index);

});

describe('GET /', function () {
    it('respond with success (200) status ', function (done) {
        request(app)
            .get('/')
            .expect(200, done);
    });
    it('respond with the techitalia name', function (done) {
        request(app)
            .get('/')
            .expect(/.*TechItalia.*/, done);
    });
});
